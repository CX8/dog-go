package ch.industries.torniamo.doggo.camera;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.AppCompatSeekBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ch.industries.torniamo.doggo.CallBack;
import ch.industries.torniamo.doggo.R;
import ch.industries.torniamo.doggo.Util;
import ch.industries.torniamo.doggo.dogs.DogView;

import static android.app.Activity.RESULT_OK;


public class CameraFragment extends Fragment {

    private ImageView v;
    private Boolean phototaken = false;
    private Button save;
    private EditText name;
    private EditText breed;
    private SeekBar size;
    private EditText comment;
    static final int REQUEST_TAKE_PHOTO = 1;
    static final int REQUEST_LOAD_PHOTO = 2;
    private String mCurrentPhotoPath;
    private Util util = new Util();
    private Button load;
    private Drawable thumb;
    private Bitmap bmpOrg;



    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        //Code executes EVERY TIME user views the fragment
        if (!isVisibleToUser) {
            clean();
        }
    }


    @Override
    public void onDetach() {
        clean();
        super.onDetach();
    }

    @Override
    public void onDestroyView()
    {
        clean();
        super.onDestroyView();

    }

    private void clean(){
        if(phototaken){
            File file = new File(mCurrentPhotoPath);
            file.delete();
            statechange(false);
        }

    }




    private void statechange(Boolean val){
        name.setEnabled(val);
        breed.setEnabled(val);
        size.setEnabled(val);
        comment.setEnabled(val);
        save.setEnabled(val);
        load.setEnabled(!val);
        v.setEnabled(!val);
        phototaken = val;
        if(!val){
            Bitmap b = BitmapFactory.decodeResource(getResources(), R.drawable.camera);
            v.setImageBitmap(b);
            name.setText("");
            breed.setText("");
            comment.setText("");
            size.setProgress(0);
        }

    }




    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.camera, container, false);
        v = (ImageView) view.findViewById(R.id.imageview);
        name = view.findViewById(R.id.DogName);
        breed = (EditText) view.findViewById(R.id.DogBreed);
        size = (SeekBar) view.findViewById(R.id.DogSize);
        comment = (EditText) view.findViewById(R.id.DogComment);
        final Resources res = getResources();
        thumb = res.getDrawable(R.drawable.dog);
        bmpOrg = ((BitmapDrawable)thumb).getBitmap();
        final int i = size.getProgress();
        Bitmap bmpScaled = Bitmap.createScaledBitmap(bmpOrg, 50+10*i, 50+10*i, true);
        Drawable newThumb = new BitmapDrawable(res, bmpScaled);
        size.setThumb(newThumb);

        size.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
                if(progress >96){
                    seekBar.setProgress(96);
                }
                Bitmap bmpScaled = Bitmap.createScaledBitmap(bmpOrg, 50+1*progress, 50+1*progress, true);
                Drawable newThumb = new BitmapDrawable(res, bmpScaled);
                size.setThumb(newThumb);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });





        Bitmap b = BitmapFactory.decodeResource(getResources(), R.drawable.camera);
        v.setImageBitmap(b);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!phototaken){takePhoto();};
            }
        });


        load = (Button) view.findViewById(R.id.load_photo);
        save = (Button) view.findViewById(R.id.savebutton);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (phototaken) {
                    File tmp = new File(mCurrentPhotoPath);
                    File saved = new File(tmp.getParentFile().getParentFile(), tmp.getName());
                    System.out.println(saved.getPath());
                    try {
                        util.copy(tmp, saved);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    tmp.delete();
                    ExifInterface exif = null;
                    Location l = util.getLastKnownLocation(getActivity());

                    try {
                        exif = new ExifInterface(saved.getPath());
                        exif.setAttribute("UserComment", name.getText() + ",.," + breed.getText() + ",.," + size.getProgress() + ",.," + comment.getText() + ",.," + l.getLatitude() + ",.," + l.getLongitude()+",.,Me");
                        exif.saveAttributes();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    //show dog view
                    util.uploadphoto(saved.getPath(), name.getText() + ",.," + breed.getText() + ",.," + size.getProgress() + ",.," + comment.getText() + ",.," + l.getLatitude() + ",.," + l.getLongitude()+",.,Me");


                    statechange(false);


                    Intent intent = new Intent(getActivity(), DogView.class);
                    String message = saved.getPath();
                    intent.putExtra(Util.EXTRA_MESSAGE, message);
                    startActivity(intent);
                }

            }
        });

        load.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadPhoto();
            }
        });

        statechange(false);

        return view;
    }


    private void loadPhoto() {

        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        System.out.println(android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto, REQUEST_LOAD_PHOTO);


    }


    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        storageDir = new File(storageDir, "tmp");
        if(!storageDir.exists()){
            storageDir.mkdir();
        }
        File image = new File(storageDir, imageFileName+".jpg");

        mCurrentPhotoPath = image.getPath();
        System.out.println(mCurrentPhotoPath);
        return image;
    }


    private void takePhoto() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            File file = null;
            try {
                file = createImageFile();
            } catch (IOException e) {
                e.printStackTrace();
            }

            Uri photoURI = FileProvider.getUriForFile(getActivity(),
                    "ch.industries.torniamo.doggo",
                    file);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);

            startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);

        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
            statechange(true);

            v.setImageBitmap(BitmapFactory.decodeFile(mCurrentPhotoPath));

        } else if (requestCode == REQUEST_LOAD_PHOTO && resultCode == RESULT_OK) {
            statechange(true);

            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            File original = new File(picturePath);

            File f =null;
            try {
                f = createImageFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                util.copy(original, f);
                v.setImageBitmap(BitmapFactory.decodeFile(mCurrentPhotoPath));

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }



}
