package ch.industries.torniamo.doggo.dogs;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import java.io.File;
import java.util.Map;

import ch.industries.torniamo.doggo.R;
import ch.industries.torniamo.doggo.Util;

/**
 * Created by chris on 01.12.17.
 */

public class DogView extends AppCompatActivity {
    private Util util = new Util();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dogview);
        Intent intent = getIntent();
        final String message = intent.getStringExtra(Util.EXTRA_MESSAGE);

        // Capture the layout's TextView and set the string as its text
        ImageView v = findViewById(R.id.imageview2);
        File file = new File(message);
        Map<String, String> infoMap = util.extratExif(file);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 1;
        options.inJustDecodeBounds = true;
        options.inSampleSize = util.calculateInSampleSize(options, v.getWidth(),v.getHeight());
        options.inJustDecodeBounds = false;
        v.setImageBitmap(BitmapFactory.decodeFile(message,options));
        TextView dogName = (TextView) this.findViewById(R.id.DogName2);
        TextView dogBreed = (TextView) this.findViewById(R.id.DogBreed2);
        TextView dogComment = (TextView) this.findViewById(R.id.DogComment2);
        SeekBar dogSize = (SeekBar) this.findViewById(R.id.DogSize2);

        dogName.setText(infoMap.get("name"));
        dogBreed.setText(infoMap.get("breed"));
        dogComment.setText(infoMap.get("comment"));
        dogSize.setProgress(Integer.parseInt(infoMap.get("size")));
        Drawable thumb = getResources().getDrawable(R.drawable.dog);
        Bitmap bmpOrg = ((BitmapDrawable)thumb).getBitmap();
        Bitmap bmpScaled = Bitmap.createScaledBitmap(bmpOrg, 50+1*dogSize.getProgress(), 50+1*dogSize.getProgress(), true);
        Drawable newThumb = new BitmapDrawable(getResources(), bmpScaled);
        dogSize.setThumb(newThumb);
        dogSize.setEnabled(false);
        Button deleteButton = (Button) findViewById(R.id.deleteButton);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                util.deletePhoto(message);
                Intent intent = new Intent();
                String message = "deleted";
                intent.putExtra(Util.EXTRA_MESSAGE, message);
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        });

    }


    @Override
    protected void onResume(){
        super.onResume();
    }
}
