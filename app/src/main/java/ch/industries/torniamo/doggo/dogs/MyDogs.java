package ch.industries.torniamo.doggo.dogs;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ch.industries.torniamo.doggo.CallBack;
import ch.industries.torniamo.doggo.R;
import ch.industries.torniamo.doggo.Util;

public class MyDogs extends Fragment implements CallBack {


    private View view;
    private Util util = new Util();
    private int size;
    private List<File> myfiles = new ArrayList<>();
    private List<String> names = new ArrayList<>();
    private List<String> owner = new ArrayList<>();
    private List<Bitmap> images = new ArrayList<>();
    private boolean computing = false;
    private ProgressDialog progressDialog;
    private CallBack callback = this;
    ListView lView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.doglist, container, false);
        size = (int) getResources().getDimension(R.dimen.dogprev);
        Button refresh = (Button) view.findViewById(R.id.refreshdogs);
        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog = ProgressDialog.show(getActivity(), getString(R.string.app_name), "Downloading doggos"); progressDialog.setCancelable(true);
                util.getUserPref( callback,0);

            }
        });
        lView = view.findViewById(R.id.recordEntriesList);
        lView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getActivity(), DogView.class);
                String message = myfiles.get(i).getAbsolutePath();
                intent.putExtra(Util.EXTRA_MESSAGE, message);
                startActivity(intent);
            }
        });

        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && !computing) {
            System.out.println("fdsakjfhjasdhfjkhasdjklfh");
            setListAdapter();
        }

    }

    @Override
    public void onResume() {
        setListAdapter();
        super.onResume();
    }

    private void setListAdapter() {
        computing = true;

        File fileDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        final List<File> files = util.getAllFiles(fileDir);
        List<Map<String, String>> infoMaps = new ArrayList<>();

        BitmapFactory.Options options = new BitmapFactory.Options();
        List<File> toRemove = new ArrayList<>(myfiles);
        toRemove.removeAll(files);
        files.removeAll(myfiles);
        for (File file : files) {

            options.inSampleSize = 1;
            options.inJustDecodeBounds = true;
            options.inSampleSize = util.calculateInSampleSize(options, size, size)*6;
            options.inJustDecodeBounds = false;

            images.add(BitmapFactory.decodeFile(file.getAbsolutePath(), options));
            Map<String, String> infoMap = util.extratExif(file);
            if (infoMap.get("name") == null) {
                names.add("unknown doggo");
            } else {
                names.add(infoMap.get("name"));
            }
            if (infoMap.get("owner") == null) {
                owner.add("unknown owner");
            } else {
                owner.add(infoMap.get("owner"));
            }
            infoMaps.add(infoMap);
            myfiles.add(file);
        }

        int removed = 0;
        for (File f : toRemove) {
            int i = myfiles.indexOf(f);
            names.remove(i-removed);
            owner.remove(i-removed);
            images.remove(i-removed);
            removed++;
        }
        myfiles.removeAll(toRemove);




        ListAdapter s = lView.getAdapter();

        DogListAdapter dogAdapter;
        if(s != null){
            dogAdapter = (DogListAdapter)s;
            dogAdapter.update( names, images, owner);

        }else{
            dogAdapter = new DogListAdapter(view.getContext(), names, images, owner);
        }

        lView.setAdapter(dogAdapter);




        computing = false;
    }


    @Override
    public void methodToCallBack(Object callbackdata, Class classType, int code){
        //download dogs
        if(code == 0){
            if(callbackdata!=null){
                List<List<String>> pref=(List<List<String>> ) callbackdata;
                if(!pref.isEmpty()){
                    util.downloadDogs0(pref,getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES).getPath(), this, 1);
                }else if(progressDialog!=null){
                    progressDialog.cancel();
                }

            }else if(progressDialog!=null){
                progressDialog.cancel();
            }}
        //done downloading dog success
        else if(code ==1 && progressDialog!=null){
            progressDialog.cancel();
            setListAdapter();

        }
    }


}
