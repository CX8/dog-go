package ch.industries.torniamo.doggo;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.provider.CalendarContract;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.view.ViewPager;
import android.widget.ImageView;
import android.widget.ListView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ch.industries.torniamo.doggo.camera.CameraFragment;
import ch.industries.torniamo.doggo.friends.FriendListAdapter;
import ch.industries.torniamo.doggo.friends.FriendsView;

/**
 * Created by chris on 30.11.17.
 */

public class Util {

    public static final String EXTRA_MESSAGE = "com.example.myfirstapp.MESSAGE";

    /**
     * extract metadata from picture
     *
     * @param f
     * @return
     */
    public Map<String, String> extratExif(File f) {
        Map<String, String> map = new HashMap<String, String>();
        ExifInterface exif = null;

        try {
            exif = new ExifInterface(f.getPath());
            String data = exif.getAttribute("UserComment");
            map = metadataToMap(data);


        } catch (IOException e) {
            e.printStackTrace();
        }

        return map;
    }

    /**
     * helper method break strings in the varous metadata
     *
     * @param metadata
     * @return
     */
    public Map<String, String> metadataToMap(String metadata) {
        Map<String, String> map = new HashMap<>();
        if (metadata == null) {
            metadata = "";
        }
        String[] datas = metadata.split(",\\.,");

        for (int i = 0; i < datas.length; i++) {
            switch (i) {
                case 0:
                    map.put("name", datas[i]);
                    break;
                case 1:
                    map.put("breed", datas[i]);
                    break;
                case 2:
                    map.put("size", datas[i]);
                    break;
                case 3:
                    map.put("comment", datas[i]);
                    break;
                case 4:
                    map.put("latitude", datas[i]);
                    break;
                case 5:
                    map.put("longitude", datas[i]);
                    break;
                case 6:
                    map.put("owner", datas[i]);
                    break;
            }

        }

        return map;

    }


    /**
     * Get last know location
     *
     * @param a
     * @return
     */
    //https://stackoverflow.com/questions/9873190/my-current-location-always-returns-null-how-can-i-fix-this
    public Location getLastKnownLocation(Activity a) {
        LocationManager locationManager = (LocationManager) a.getSystemService(Context.LOCATION_SERVICE);

        List<String> providers = locationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            if (ActivityCompat.checkSelfPermission(a, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(a, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return null;
            }
            Location l = locationManager.getLastKnownLocation(provider);


            if (l == null) {
                continue;
            }
            if (bestLocation == null
                    || l.getAccuracy() < bestLocation.getAccuracy()) {
                bestLocation = l;
            }
        }
        if (bestLocation == null) {
            return null;
        }
        return bestLocation;
    }


    /**
     * Duplicate file when importing from gallery
     *
     * @param src
     * @param dst
     * @throws IOException
     */

    //https://stackoverflow.com/questions/9292954/how-to-make-a-copy-of-a-file-in-android
    public void copy(File src, File dst) throws IOException {
        InputStream in = null;
        OutputStream out = null;
        try {
            in = new FileInputStream(src);
            out = new FileOutputStream(dst);
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
        } finally {
            if (in != null) in.close();
            if (out != null) out.close();
        }

    }


    /**
     * save photo to cloud
     *
     * @param path
     * @param metadata
     */
    public void uploadphoto(final String path, final String metadata) {
        //TODO resize before upload if enough time
        final File file = new File(path);

        final FirebaseAuth mAuth = FirebaseAuth.getInstance();
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReference();
        final StorageReference mountainsRef = storageRef.child(mAuth.getCurrentUser().getEmail()).child(file.getName());
        Uri u = Uri.fromFile(file);
        UploadTask uploadTask = mountainsRef.putFile(u);

        // Register observers to listen for when the download is done or if it fails
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful uploads
                System.out.println(exception.getMessage());
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                saveUserPhotoDB(mAuth.getCurrentUser().getUid(), mountainsRef.getPath(), metadata, file.getName());
            }
        });
    }

    /**
     * save picture data on db
     *
     * @param useUID
     * @param path
     * @param metadata
     */
    private void saveUserPhotoDB(final String useUID, final String path, final String metadata, final String name) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = database.getReference("fotos").child(useUID);
        final List<List<String>> photos = new ArrayList<>();
        photos.add(new ArrayList<String>(Arrays.asList(path, metadata, name)));

        myRef.addListenerForSingleValueEvent((new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot.getValue() != null) {
                    photos.addAll((List<List<String>>) dataSnapshot.getValue());
                }
                //call static method where pref are needed
                myRef.setValue(photos);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("dsafasdlfdddddddddk");

            }

        }));
    }



    /**
     * delete photo from cloud storage
     *
     * @param path
     */
    public void deletePhoto(final String path) {
        final File file = new File(path);
        final Map<String, String> stringStringHashMap = extratExif(file);



        if(stringStringHashMap.get("owner").equals("Me")) {
            final FirebaseAuth mAuth = FirebaseAuth.getInstance();
            FirebaseStorage storage = FirebaseStorage.getInstance();
            StorageReference storageRef = storage.getReference();
            StorageReference mountainsRef = storageRef.child(mAuth.getCurrentUser().getEmail()).child(file.getName());

            mountainsRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {

                    deleteUserPhotoDB(mAuth.getCurrentUser().getUid(), mAuth.getCurrentUser().getEmail() + "/" + file.getName());

                    // File deleted successfully
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Uh-oh, an error occurred!
                }
            });
        }
        file.delete();
    }

    /**
     * delete photo data from bb
     *
     * @param getUid
     * @param path
     */
    private void deleteUserPhotoDB(final String getUid, final String path) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = database.getReference("fotos").child(getUid);
        final List<List<String>> photos = new ArrayList<>();

        myRef.addListenerForSingleValueEvent((new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {
                    photos.addAll((List<List<String>>) dataSnapshot.getValue());
                }
                //call static method where pref are needed
                deletecallbackdb(photos, myRef, path);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("dsafasdlfdddddddddk");

            }

        }));
    }

    /**
     * delete photo data from db callback
     *
     * @param photos
     * @param ref
     * @param path
     */
    private void deletecallbackdb(List<List<String>> photos, DatabaseReference ref, String path) {
        List<String> toremove = new ArrayList<>();
        for (List<String> l : photos) {
            for (String s : l) {

                if (s.equals("/"+path)) {
                    toremove = l;
                }
            }
        }
        photos.remove(toremove);
        ref.setValue(photos);
    }


    /**
     * save user folowers
     *
     * @param usermail
     */
    public void saveUserPref(final String usermail, final String userid) {
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = database.getReference("following").child(mAuth.getCurrentUser().getUid());
        final List<List<String>> users = new ArrayList<>();
        users.add(Arrays.asList(userid,usermail));

        myRef.addListenerForSingleValueEvent((new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot.getValue() != null) {
                    users.addAll((List<List<String>>) dataSnapshot.getValue());
                }
                myRef.setValue(users);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("dsafasdlfdddddddddk");

            }

        }));
    }



    /**
     * remove user from following
     *
     * @param usermail
     */

    public void deleteUserPref(final String usermail) {
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = database.getReference("following").child(mAuth.getCurrentUser().getUid());
        final List<List<String>> users = new ArrayList<>();

        myRef.addListenerForSingleValueEvent((new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {
//                    System.out.println("not null");

                    users.addAll((List<List<String>>) dataSnapshot.getValue());
                }
                deletePrefcallback(users, usermail, myRef);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("dsafasdlfdddddddddk");

            }

        }));
    }

    /**
     * remove user frm following helper
     *
     * @param users
     * @param mail
     * @param ref
     */
    private void deletePrefcallback(List<List<String>> users, String mail, DatabaseReference ref) {
        List<String> toremove = new ArrayList<>();
        for (List<String> l : users) {
            for (String s : l) {
                if (s.equals(mail)) {
                    toremove = l;
                }
            }
        }
        users.remove(toremove);
        ref.setValue(users);
    }




    /**
     * get users you follow
     *
     * @param callback
     * @param code
     */
    public void getUserPref( final CallBack callback ,final int code) {
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = database.getReference("following").child(mAuth.getCurrentUser().getUid());
        final List<List<String>> users = new ArrayList<>();

        myRef.addListenerForSingleValueEvent((new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {
//                    System.out.println("not null");

                    users.addAll((List<List<String>>) dataSnapshot.getValue());
                }

                callback.methodToCallBack(users,users.getClass(),code);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("dsafasdlfdddddddddk");

            }
        }));

    }


    /**
     * download picture
     *
     * @param preferences
     */
    public void downloadDogs0( final List<List<String>> preferences, final String storageDir, final CallBack callBack, final int code) {

        FirebaseDatabase database = FirebaseDatabase.getInstance();



        for(final List<String> singlepref : preferences) {
            final boolean last;
            if(preferences.indexOf(singlepref) == preferences.size()-1){last = true;}else{last = false;}
            DatabaseReference myRef = database.getReference("fotos").child(singlepref.get(0));
            final List<List<String>> photos = new ArrayList<>();

            myRef.addListenerForSingleValueEvent((new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
//                System.out.println(dataSnapshot.getValue());

                    if (dataSnapshot.getValue() != null) {
//                    System.out.println("not null");
                        photos.addAll((List<List<String>>) dataSnapshot.getValue());
                    }
                    dowloadDogs2(photos, singlepref.get(1), storageDir, callBack, code, last);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    System.out.println("dsafasdlfdddddddddk");

                }

            }));
        }

    }



    private  void dowloadDogs2(List<List<String>> photos, final String usermail, String storageDir, final CallBack callBack, final int code, final boolean last){
        FirebaseStorage storage = FirebaseStorage.getInstance();
        File folf = new File(storageDir+"/"+usermail);
        if(!folf.exists()){
            folf.mkdir();
        }
        int tot = 0;
        int count = 0;

        File f;
        //count photos to download
        List<File> allphotos = new ArrayList<>();
        for (final List<String> l : photos){
            f = new File(folf,l.get(2));
            allphotos.add(f);
            if(!f.exists()){
                tot++;
            }
        }

        for (final File file : allphotos){
            final List<String> l = photos.get(allphotos.indexOf(file));


            if(!file.exists()){
                count++;
                System.out.println("new FILE");
                StorageReference myRef = storage.getReference().child(l.get(0));


                final int finalCount = count;
                final int finalTot = tot;
                myRef.getFile(file).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                        try {
                            ExifInterface exif = new ExifInterface(file.getPath());
                            Map<String,String> map = metadataToMap(l.get(1));

                            exif.setAttribute("UserComment", map.get("name")+ ",.," +map.get("breed")+ ",.," +map.get("size")+ ",.," +map.get("comment")+ ",.," +map.get("latitude")+ ",.," +map.get("longitude")+",.,"+usermail);
                            exif.saveAttributes();

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }).addOnCompleteListener(new OnCompleteListener<FileDownloadTask.TaskSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<FileDownloadTask.TaskSnapshot> task) {
                        if(finalCount == finalTot && last){
                            callBack.methodToCallBack(null,null,code);
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle any errors
                    }
                });


            }else{
                System.out.println("old FILE");

            }


        }
        if(tot ==0){
            callBack.methodToCallBack(null,null,code);
        }

    }



    public void getUsers(final CallBack callBack, final int code){
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("users");
        final List<List<String>> users = new ArrayList<>();
        myRef.addListenerForSingleValueEvent((new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
//                System.out.println(dataSnapshot.getValue());
                users.addAll((List<List<String>>)dataSnapshot.getValue());
                callBack.methodToCallBack(users,users.getClass(),code);


            }

            @Override
            public void onCancelled(DatabaseError databaseError){

            }

        }));
    }

    public List<File> getAllFiles(File root){
        List<File> files = new ArrayList<>();
        final File[] tmpFiles = root.listFiles();
        for (int i = 0; i < tmpFiles.length; i++) {
            if(!tmpFiles[i].isDirectory()){
                files.add(tmpFiles[i]);
            }else if(!tmpFiles[i].getName().contains("tmp")){
                files.addAll(getAllFiles(tmpFiles[i]));
            }
        }
        return files;
    }


    public int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

}
