package ch.industries.torniamo.doggo;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


//public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
public class MainActivity extends AppCompatActivity implements CallBack {

    CollectionPagerAdapter collectionPagerAdapter;
    ViewPager viewPager;
    private Util util = new Util();
    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //get user pref to dowload doggos
        progressDialog = ProgressDialog.show(this, getString(R.string.app_name), "Downloading doggos"); progressDialog.setCancelable(true);
        util.getUserPref( this,0);

        //delete temp directory if tmp pictures are left behind
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        storageDir = new File(storageDir, "tmp");
        if(storageDir.exists()){
            storageDir.delete();
        }


        collectionPagerAdapter = new CollectionPagerAdapter(getSupportFragmentManager());
        viewPager = (ViewPager) findViewById(R.id.pager);
        viewPager.setAdapter(collectionPagerAdapter);

    }

    //suppress backpress in fragment view
    @Override
    public void onBackPressed() {

        int count = getFragmentManager().getBackStackEntryCount();

        if (count == 0) {
//            super.onBackPressed();
            if(
            viewPager.getCurrentItem()!=0) {
                viewPager.setCurrentItem( viewPager.getCurrentItem()-1, true );
            }

            //additional code
        } else {
            getFragmentManager().popBackStack();
        }

    }

    @Override
    protected void onResume(){
        super.onResume();

        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
        }

        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 2);
        }
    }

    public void methodToCallBack(Object callbackdata, Class classType, int code){
        //download dogs
        if(code == 0){
            if(callbackdata!=null){
                List<List<String>> pref=(List<List<String>> ) callbackdata;
                if(!pref.isEmpty()){
                    util.downloadDogs0(pref,getExternalFilesDir(Environment.DIRECTORY_PICTURES).getPath(), this, 1);
                }else if(progressDialog!=null){
                    progressDialog.cancel();
                }

            }else if(progressDialog!=null){
                progressDialog.cancel();
            }}
        //done downloading dog success
        else if(code ==1 && progressDialog!=null){
            progressDialog.cancel();

        }
    }
}
