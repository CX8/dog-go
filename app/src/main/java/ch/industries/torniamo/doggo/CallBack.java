package ch.industries.torniamo.doggo;

import java.util.List;

/**
 * Created by chris on 14.12.17.
 */

public interface CallBack {
    void methodToCallBack(Object callbackdata, Class classType, int code);
}