package ch.industries.torniamo.doggo.login;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


import com.google.firebase.auth.FirebaseAuth;

import ch.industries.torniamo.doggo.R;

public class WelcomeFragment extends Fragment {

    private Button login;
    private static View view;


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.welcome, container, false);

        //Button saveButton = (Button) view.findViewById(R.id.save);
        //Button deleteButton = (Button) view.findViewById(R.id.delete);
        String userName = FirebaseAuth.getInstance().getCurrentUser().getEmail();
        TextView userNameView = (TextView) view.findViewById(R.id.username);
        userNameView.setText(userName);
        Button logoutButton = (Button) view.findViewById(R.id.logout);

        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseAuth.getInstance().signOut();
//                Intent intent = new Intent(getActivity(), LoginActivity.class);
//                startActivity(intent);
                getActivity().finish();
            }
        });
//        final EditText text = (EditText) view.findViewById(R.id.data);
//
//        saveButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Util.saveUserPref(text.getText().toString());
//            }
//        });
//        deleteButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Util.deleteUserPref(text.getText().toString());
//
//            }
//        });
//        getButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Util.getUserPref(2, null);
//            }
//        });

        return view;
    }


}
