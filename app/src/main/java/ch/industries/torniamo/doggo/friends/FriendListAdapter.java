package ch.industries.torniamo.doggo.friends;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ch.industries.torniamo.doggo.R;

/**
 * Created by met on 14.12.17.
 */

public class FriendListAdapter extends ArrayAdapter<String> {


    private final Context context;
    private final List<String> users;
    private List<String> userPreferences;
    private final Bitmap check;


    public FriendListAdapter(Context context, List<String> users, List<String> prefUsers) {
        super(context, R.layout.dog_list_item, users);
        this.context = context;
        this.users = users;
        this.userPreferences = prefUsers;
        this.check = BitmapFactory.decodeResource(context.getResources(), R.drawable.check);
    }

    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View rowView=inflater.inflate(R.layout.friend_list_item, null,true);

        TextView friendName = (TextView) rowView.findViewById(R.id.friendTextView);
        ImageView dogImage = (ImageView) rowView.findViewById(R.id.checkImg);

        friendName.setText(users.get(position));
        if(userPreferences.contains(users.get(position))){
            dogImage.setImageBitmap(check);
        }
        return rowView;

    };
}
