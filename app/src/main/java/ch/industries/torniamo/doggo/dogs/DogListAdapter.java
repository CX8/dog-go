package ch.industries.torniamo.doggo.dogs;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ch.industries.torniamo.doggo.R;

/**
 * Created by Matteo Morisoli on 29.11.17.
 */

public class DogListAdapter extends ArrayAdapter<String> {


    private List<String> names;
    private List<String> owners;
    private List<Bitmap> images;


    public DogListAdapter(Context context, List<String> names, List<Bitmap> images, List<String> owners) {
        super(context, R.layout.dog_list_item, names);
        this.owners = owners;
        this.images = images;
        this.names = names;
    }

    public void update(List<String> names, List<Bitmap> images, List<String> owners) {

        this.owners = owners;
        this.images = images;
        this.names = names;
        super.notifyDataSetChanged();

    }

    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View rowView = inflater.inflate(R.layout.dog_list_item, null, true);

        TextView dogName = (TextView) rowView.findViewById(R.id.dogTextView);
        TextView dogOwner = (TextView) rowView.findViewById(R.id.dogTextViewOwner);
        ImageView dogImage = (ImageView) rowView.findViewById(R.id.dogImg);

        dogName.setText(names.get(position));
        dogOwner.setText("Snapped by: "+owners.get(position));
        dogImage.setImageBitmap(images.get(position));
        return rowView;

    }


    ;
}
