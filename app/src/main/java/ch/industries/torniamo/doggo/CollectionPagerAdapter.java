package ch.industries.torniamo.doggo;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import ch.industries.torniamo.doggo.camera.CameraFragment;
import ch.industries.torniamo.doggo.dogs.MyDogs;
import ch.industries.torniamo.doggo.friends.FriendsView;
import ch.industries.torniamo.doggo.maps.MyMapFragment;
import ch.industries.torniamo.doggo.login.WelcomeFragment;



public class CollectionPagerAdapter extends FragmentStatePagerAdapter {
    public CollectionPagerAdapter(FragmentManager fm) {
        super(fm);
    }



    private static int NUMBER_OF_FRAGMENTS = 5;
    @Override
    public Fragment getItem(int position) {
        Fragment fragment;
        switch (position) {
            case 0:
                fragment =  new WelcomeFragment();
                return fragment;
            case 1:
                fragment =  new MyDogs();
                return fragment;
            case 2:
                fragment = new MyMapFragment();
                return fragment;
            case 3:
                fragment = new CameraFragment();
                return fragment;
            case 4:
                fragment = new FriendsView();
                return fragment;
            default:
                return null;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Welcome";
            case 1:
                return "My Dogs";
            case 2:
                return "Map";
            case 3:
                return "Camera";
            case 4:
                return "Friends";
            default:
                return "EMPTY TAB";
        }
    }

    @Override
    public int getCount() {
        return NUMBER_OF_FRAGMENTS;
    }
}
