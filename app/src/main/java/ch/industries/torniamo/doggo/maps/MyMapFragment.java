package ch.industries.torniamo.doggo.maps;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ch.industries.torniamo.doggo.R;
import ch.industries.torniamo.doggo.Util;
import ch.industries.torniamo.doggo.dogs.DogView;

public class MyMapFragment extends Fragment implements OnMapReadyCallback {


    private static View view;
    private GoogleMap googleMapInstance;
    private HashMap<Marker, String> filefrommarker = new HashMap<>();
    private Util util = new Util();
    private Bitmap b2;
    private Bitmap b;
    //so many duplications...
    private List<File>myfiles = new ArrayList<>();
    private List<Marker>mymarkers = new ArrayList<>();
    private HashMap<Marker, MarkerOptions>mymarkersmap = new HashMap<>();


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        b = BitmapFactory.decodeResource(getResources(), R.drawable.dog);
        b = Bitmap.createScaledBitmap(b, 80, 80, false);
        b2 = BitmapFactory.decodeResource(getResources(), R.drawable.dog2);
        b2 = Bitmap.createScaledBitmap(b, 80, 80, false);

        try {
            view = inflater.inflate(R.layout.maps, container, false);
        } catch (InflateException e) {
        }
        ((MapFragment) getActivity().getFragmentManager().findFragmentById(R.id.mapsfragment)).getMapAsync(this);

        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            loadDogs(false);
        }

    }

    @Override
    public void onResume() {

        loadDogs(false);
        super.onResume();
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMapInstance = googleMap;
        googleMap.clear();

        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED) {
            googleMap.setMyLocationEnabled(true);
            // Animate to current location
            Location currentLocation = util.getLastKnownLocation(getActivity());
            float zoomLevel = 12;
            Location l = util.getLastKnownLocation(getActivity());
            if (l != null) {
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(l.getLatitude(), l.getLongitude()), zoomLevel));
            }
        }
        googleMapInstance.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                Intent intent = new Intent(getActivity(), DogView.class);
                String message = filefrommarker.get(marker);
                intent.putExtra(Util.EXTRA_MESSAGE, message);
                startActivity(intent);
            }
        });

        loadDogs(true);

    }

    private void loadDogs(Boolean toZoom) {





        if (googleMapInstance != null) {
            File fileDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            List<File> files = util.getAllFiles(fileDir);
            List<File> toremove = new ArrayList<>(myfiles);
            toremove.removeAll(files);
            files.removeAll(myfiles);

            for (File file : files) {
                myfiles.add(file);

                Map<String, String> data = util.extratExif(file);
                //check if the picture has all the metadata
                if (data.keySet().size() == 7) {

                    LatLng loc = new LatLng(Double.parseDouble(data.get("latitude")), Double.parseDouble(data.get("longitude")));


                    String name = data.get("name");
                    if(name.equals("")){
                        name = "no name";
                    }
                    MarkerOptions m;
                    if (!data.get("owner").equals("Me")) {
                        m = new MarkerOptions()
                                .position(loc).title(name).snippet(data.get("comment"))
                                .icon(BitmapDescriptorFactory.fromBitmap(b));
                    } else {
                        m = new MarkerOptions()
                                .position(loc).title(name).snippet(data.get("comment"))
                                .icon(BitmapDescriptorFactory.fromBitmap(b));
                    }


                    Marker marker = googleMapInstance.addMarker(m);
                    filefrommarker.put(marker, file.getPath());
                    mymarkersmap.put(marker, m);
                    mymarkers.add(marker);
                }
            }
            int offset = 0;
            for(File f : toremove){
                int i = myfiles.indexOf(f);
                Marker m = mymarkers.get(i);
                filefrommarker.remove(m);
                mymarkersmap.remove(m);
                m.remove();
                mymarkers.remove(i-offset);
                offset++;
            }
            myfiles.removeAll(toremove);
            if(!toremove.isEmpty()){
                System.out.println("sadfasdf111111");

                googleMapInstance.clear();
                for(Marker m : mymarkers){
                    googleMapInstance.addMarker(mymarkersmap.get(m));
                }
            }
            if (toZoom && !mymarkers.isEmpty()) {
                //set map to include all the dog markers
                LatLngBounds.Builder builder = new LatLngBounds.Builder();

                for (Marker marker : mymarkers) {
                    builder.include(marker.getPosition());
                }
                LatLngBounds bounds = builder.build();
                int width = getResources().getDisplayMetrics().widthPixels;
                int height = getResources().getDisplayMetrics().heightPixels;
                int padding = (int) (width * 0.20); // offset from edges of the map % of screen
                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
                googleMapInstance.animateCamera(cu);
            }
        }
    }
}


