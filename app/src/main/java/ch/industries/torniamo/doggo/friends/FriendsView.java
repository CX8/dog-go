package ch.industries.torniamo.doggo.friends;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ch.industries.torniamo.doggo.CallBack;
import ch.industries.torniamo.doggo.R;
import ch.industries.torniamo.doggo.Util;



public class FriendsView extends Fragment implements CallBack {


    private View view;
    private List<List<String>> allUsers;
    private List<List<String>> following;

    private ListView listView;
    private Util util = new Util();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.friendsview, container, false);
        listView = view.findViewById(R.id.friendsEntriesList);
        Button refresh = (Button) view.findViewById(R.id.refresh);
        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setFriendListAdapter();
            }
        });
        setFriendListAdapter();
        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
//            setFriendListAdapter();
        }

    }

    @Override
    public void onResume() {
//        setFriendListAdapter();
        super.onResume();
    }



    private void setFriendListAdapter() {
        util.getUsers(this, 0);

    }


    @Override
    public void methodToCallBack(Object callbackdata, Class classType, int code) {
        if (code == 0) {
            allUsers = (List<List<String>>) callbackdata;
            allUsers.remove(Arrays.asList(FirebaseAuth.getInstance().getCurrentUser().getUid(), FirebaseAuth.getInstance().getCurrentUser().getEmail()));
            util.getUserPref(this, 1);

        } else if (code == 1) {
            following = (List<List<String>>) callbackdata;

            final List<String> allUsersSimple = new ArrayList<>();
            final List<String> followingSimple = new ArrayList<>();
            for (List<String> list : allUsers) {
                allUsersSimple.add(list.get(1));
            }
            for (List<String> list : following) {
                followingSimple.add(list.get(1));
            }

            ArrayAdapter<String> friendListAdapter = new FriendListAdapter(view.getContext(), allUsersSimple, followingSimple);


            listView.setAdapter(friendListAdapter);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    if (followingSimple.contains(allUsersSimple.get(i))) {
                        util.deleteUserPref(allUsersSimple.get(i));
                    } else {
                        util.saveUserPref(allUsersSimple.get(i), allUsers.get(i).get(0));
                    }
                    setFriendListAdapter();
                }
            });
        }

    }
}
